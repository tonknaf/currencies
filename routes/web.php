<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (\Illuminate\Http\Request $request) {
    $client = new \GuzzleHttp\Client();

    $res = $client->request('GET', 'http://phisix-api3.appspot.com/stocks.json');

    if ($res->getStatusCode() !== 200) {
        throw new \Illuminate\Http\Exceptions\HttpResponseException(
            new \Illuminate\Http\Response(
                'Remote service unavailable',
                \Illuminate\Http\Response::HTTP_SERVICE_UNAVAILABLE
            )
        );
    }

    $currencies = json_decode($res->getBody(), true)['stock'];


    return Response::view('currencies', compact('currencies'))
        ->header("Refresh", "15; url=".$request->getUri());
});
