<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Currencies</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/">Currencies</a>
            <div class="navbar-nav mr-auto">

            </div>
            <form class="form-inline my-2" action="/" method="GET">
                <a class="btn btn-outline-success my-2 my-sm-0" href="/">Refresh</a>
            </form>
        </nav>

        <section class="container-fluid">
            <table class="table table-striped table-hover table-bordered">
                <thead class="thead-dark">
                    <th scope="col">
                        Name
                    </th>
                    <th scope="col" class="price-col">
                        Price
                    </th>
                    <th scope="col" class="volume-col">
                        Volume
                    </th>
                </thead>
                <tbody>
                    @foreach ($currencies as $cur)
                        <tr>
                            <td>{{ $cur['name'] }}</td>
                            <td class="price-col">{{ number_format($cur['price']['amount'], 2) }} ₱</td>
                            <td class="volume-col">{{ number_format($cur['volume'], 0, '.', ' ') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </section>
    </body>
</html>
